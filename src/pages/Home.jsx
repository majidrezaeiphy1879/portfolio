import Container from "react-bootstrap/Container";
import ListGroup from "react-bootstrap/ListGroup";
import Spinner from "react-bootstrap/Spinner";
import Alert from "react-bootstrap/Alert";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useQuery } from "react-query";
import Card from "react-bootstrap/Card";
import Figure from "react-bootstrap/Figure";
import describe from "../images/bullseye.svg";

export default function Home() {
  
  const {
    data: posts,
    isLoading,
    isError,
    error,
    isSuccess,
  } = useQuery(["posts"], () =>
    fetch("https://www.reddit.com/r/mathematics/.json?limit=13").then(
      (response) => response.json()
    )
  );
  // const [apiitem, setApiItem]=useState([posts])
  // const handleMouseOver=(id)=>{
  //    console.log(posts.data.children.map(item=>(item.data.selftext)))
  //   const newData = posts.data.children.map(item=>{
  //     if(item.data.id===id){
  //       console.log(item.data.selftext)
  //     }
  //   })

  return (
    <div>
      <Container fluid>
        <Row>
          <Col></Col>
          <Col xs={6}>
            <Card className="card-style">
              <Card.Body>
                <Card.Title
                  style={{
                    color: "green",
                    textAlign: "center",
                    fontWeight: "bolder",
                    marginBottom: "15px",
                  }}
                >
                  Mathematics subreddit
                </Card.Title>
                {isLoading && <Spinner animation="border" />}
                {isSuccess &&
                  posts.data.children.map((post) => (
                    <ListGroup key={post.data.id}>
                      <ListGroup.Item className="api-list-items">
                        <a
                          href={`https://reddit.com/${post.data.permalink}`}
                          target="_blank"
                          rel="noreferrer"
                          className="api-link-style"
                        >
                          {post.data.title.length < 90
                            ? post.data.title
                            : post.data.title.slice(0, 90) + "..."}
                        </a>

                        <Figure.Image
                          width={20}
                          height={20}
                          alt="171x180"
                          src={describe}
                          style={{ float: "right" }}
                        />
                      </ListGroup.Item>
                    </ListGroup>
                  ))}
                {isError && <Alert variant="danger">{error.message}</Alert>}
              </Card.Body>
            </Card>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    </div>
  );
}
