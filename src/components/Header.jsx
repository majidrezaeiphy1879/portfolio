import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Link } from "react-router-dom";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Figure from "react-bootstrap/Figure";
import icon from "../images/undraw_mathematics_-4-otb.svg";
export default function Header() {
  return (
    <div>
      <Container fluid>
        <Row>
          <Col></Col>
          <Col xs={6}>
            <Navbar expand="lg">
              <Container>
                <Navbar.Brand>
                  <Figure.Image
                    width={60}
                    height={60}
                    alt="171x180"
                    src={icon}
                  />
                </Navbar.Brand>
                <Nav>
                  <Link className="nav-style" to="/">
                    Home
                  </Link>
                  <Link className="nav-style" to="about">
                    About
                  </Link>
                  <Link className="nav-style" to="contact">
                    Contact
                  </Link>
                </Nav>
              </Container>
            </Navbar>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    </div>
  );
}
