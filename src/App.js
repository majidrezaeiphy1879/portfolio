import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/About";
import Header from "./components/Header";
import Contact from "./pages/Contact";
import Bg from "./Sketch";
import { useState } from "react";
import { WebContext } from "./context/WebContext";
function App() {
  return (
   <WebContext.Provider value={{}}>
      <div className="App">
        <BrowserRouter>
          <Header />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="about" element={<About />} />
            <Route path="contact" element={<Contact />} />
          </Routes>
        </BrowserRouter>
        <Bg />
      </div>
      </WebContext.Provider>
  );
}

export default App;
